package me.jvt.cucumber.gherkinformatter;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class StringPadderTest {
  @Test
  void whenSameLength() {
    String actual = StringPadder.rightPad("testme", 6);

    assertThat(actual).isEqualTo("testme");
  }

  @Test
  void whenLessThanLenth() {
    String actual = StringPadder.rightPad("testme", 1);

    assertThat(actual).isEqualTo("testme");
  }

  @Test
  void whenPaddingRequired() {
    String actual = StringPadder.rightPad("the", 4);

    assertThat(actual).isEqualTo("the ");
  }

  @Test
  void whenLotsOfPaddingRequired() {
    String actual = StringPadder.rightPad("the", 20);

    assertThat(actual).isEqualTo("the                 ");
  }
}
